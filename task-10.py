#Дано ціле число в діапазоні 100-999. Вивести рядок-опис даного числа, наприклад: 256 - "двісті п'ятдесят шість", 814 - "вісімсот чотирнадцять». 

x = int(input("Введи ціле число в діапазоні від 100 до 999: "))

if x < 100 or x > 999:
    print("Число не входить у діапазон від 100 до 999")
    exit()

units = ["", "один", "два", "три", "чотири", "п'ять", "шість", "сім", "вісім", "дев'ять"]
teens = ["", "одинадцять", "дванадцять", "тринадцять", "чотирнадцять", "п'ятнадцять", "шістнадцять", "сімнадцять", "вісімнадцять", "дев'ятнадцять"]
tens = ["", "десять", "двадцять", "тридцять", "сорок", "п'ятдесят", "шістдесят", "сімдесят", "вісімдесят", "дев'яносто"]
hundreds = ["", "сто", "двісті", "триста", "чотириста", "п'ятсот", "шістсот", "сімсот", "вісімсот", "дев'ятсот"]

u = x % 10
t = x // 10 % 10
h = x // 100

#out = str("")

out = hundreds[h] + " "

if t == 1: 
    out += teens[u]
    print(f"Число {x} пишеться як: {out}")
else:
    out += tens[t] + " " + units[u]
    print(f"Число {x} пишеться як: {out}")