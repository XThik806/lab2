#Дано ціле число. Вивести його рядок-опис виду «від'ємне парне число», «нульове число», «додатне непарне число» і т. д.

x = int(input("Введи число: "))

even = "парне "
notEven = "не парне "

positive = "додатнє "
negative = "від'ємне "

zero = "нульове "

number = "число"

out = str("")

if x > 0 :
    out += positive

elif x == 0: 
    print(f"Число {x} це {zero + number}")
    exit()

else :
    out += negative


if x%2 == 0 :
    out = out + even + number
    print(f"Число {x} це {out}")

else :
    out = out + notEven + number
    print(f"Число {x} це {out}")  