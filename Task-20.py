#Дано ціле число, яке лежить в діапазоні 1-999.  Вивести його рядок-опис виду «парне двозначне число», «непарне тризначне число» і т. Д.

x = int(input("Введи число в діапазоні від 1 до 999: "))

if x>999:
    print("Число завелике")
    exit()

even = "парне "
notEven = "не парне "

sDigit = "однозначне "
dDigit = "двозначне "
tDifit = "трьозначне "


number = "число"

out = str("")

out += even if x%2 == 0 else notEven

if x < 10:
    out += sDigit + number
elif x < 100 and x > 10:
    out += dDigit + number
else:
    out += tDifit + number

print(f"Число {x} є {out}")